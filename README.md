# Shell Exercices


## Usage

> Clone this repo to your local machine using 

```shell
$ git clone https://gitlab.com/h3hitema/python/exercices-shell.git
```

> access the cloned repository
```shell
$ cd shell
```

> add the execution rights to all directory files

```shell
$ chmod +x ./*
```